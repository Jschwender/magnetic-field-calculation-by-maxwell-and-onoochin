# Makefile for password generator
# THVV 06/01/94 initial
# THVV 05/29/02 updated to build on Linux
# THVV 12/24/03 updated to build on gcc 3.3 (gets warnings on Panther, FreeBSD, and RH9)
#

DEBUGARGS = -g
#COMPILER = c++
CC=g++

all : 
	g++ -O3 $(DEBUGARGS)  -o elliptic-integral elliptic-integral.cpp -lquadmath
	g++ -O3 $(DEBUGARGS)  -o Multilayer-coil Multilayer-coil.cpp -lquadmath
	