# magnetic field calculation by Maxwell and Onoochin

## Getting started

Historically, calculating the induction of coils was done by empirical formulars.
There exist many formulars, most of them are limited in their application to certain geometry and the precision is not very good.
Maxwell has laid the foundation for numeric calculation by means of elliptic integrals.
Today, numeric math is feasible with all the calculation power around, but it has shown that the precision of this method also has 
limits. Vladimir Onoochin suggested an improvement of the maxwell integration, which seems to get better results for 
larger distances of wire couples. This approach is implemented here.

## Test and Deploy

There are two different implementation, AGM method and plain numeric integration.

## Installation
This is C/C++, you run make, that's it.
Prerequisites are a built environemnt with gcc and quadmath library, which should be standard on most linux distributions.

## Usage
This is just a proof of concept.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
No idea.

## Authors and acknowledgment
inspired by https://www.researchgate.net/publication/267270206
and
Vladimir Onoochin who suggested an improvement on Maxwells elliptic integral calculation for magnetic field.

## License
GNU public license Version 2.

## Project status
WIP

