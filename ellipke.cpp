#include <malloc.h>
#include <math.h>
/*
  using quad math here is necessary for getting high precision even for c close to -1 and 1.
  With double the precision is limieted to something like 1E-17.
*/
#include <quadmath.h>
#define float128 __float128

#define PI 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899
/*
   This function calculates both K and E values for a given c by using AGM Method
   This method is extremely efficient and gets precise results with 1…6 recursions, if c is not too close to 1.
   The return values for -1 and 1 are hardcoded as the calculation would fail.
*/
void K_E_calc_AGM(float128 c,float128 &Kk,float128 &Ek);
/*
   This functions are alternative implementations for the elliptic integrals.
   The method here is plain numeric integration by adding small steps in large number of loop passes.
   N is number of loop passes for intetgration. This is just a starting point, 
   if k gets close to 1 the number is increased in a kind of comporomise between runtime and precision.
*/
long N=100; 
float128 Kcalc(float128 k);
float128 Ecalc(float128 k);

/*
Elliptic integrals of the first kind are defined as

         1
        /               dt
K (m) = |------------------------------
        / sqrt ((1 - t^2)*(1 - m*t^2))
       0

Elliptic integrals of the second kind are defined as

         1
        /  sqrt (1 - m*t^2)
E (m) = | ------------------ dt
        /  sqrt (1 - t^2)
       0
*/
void K_E_calc_AGM(float128 k,float128 &Kk,float128 &Ek)
{
    float128 a,b,a1,b1,E,n;
    long i;
      a=1;n=0;
//    b=sqrtq(1-powq(k,2));
//    E=1-powq(k,2)/2;
    b=sqrtq(1-k);
    E=1-k/2;
    if (k==1.0) 
    {
       Kk=0; Ek=1;
//       printresult(k,a,b,n);
       return;
    }
    if (k==-1.0) {
       Kk=1.3110287771460599052324197949455597068413774757158115814084108519;
       Ek=1.9100988945138560089523810410857216459549838073236373605402483283;
//       printresult(k,a,b,n);
       return;
    }
    i=1;
    do
    {
	a1=(a+b)/2.0;
	b1=sqrtq(a*b);
        E=E-i*powq((a-b)/2,2);
        i=i*2;
	a=a1;
	b=b1;
	if (n>=312) break;  // this should never happen, but if for any reason the calculation does not convert we terminate the loop
	n++;
    }
    while (fabsq(a-b)>1e-62);
//       printresult(k,a,b,n);
    Kk=PI/(2*a);
    Ek=E*Kk;
}

//Complete elliptic integral of the 1st kind K(k)
float128 Kcalc(float128 k)//-1<=k<=1
{
    float128 sum=0.0,x,delta;
    long i;
    if (k==1) return 0;
//    k*=k;//k squared
    if (N>=0) N=1000+powq(10,(-logq(1-k))); // adjust number of nested loops to keep precision with k approaching 1.000000. Watch the runtime!!
    if (N>=100000) N=100000;
    if (N<=0) N=1000000;           // otherwise the loop does not end
    delta=0.5*PI/N;
    for (i=0;i<=N;i++)
	{
	    x=sinq(i*delta);
	    sum+=1.0/sqrtq(1-k*powq(x,2));
	}
    sum=sum-0.5*(1.0/sqrtq(1-k*powq(x,2)) + 1.0);
    sum=sum*delta;
    return sum;
}
//Complete elliptic integral of the 2nd kind E(k)
float128 Ecalc(float128 k)//-1<=k<=1
{
    float128 sum=0.0,x,delta;
    long i;
    if (k==1) return 1;
//    k*=k;//k squared
    if (N>=0) N=1000+powq(10,(-logq(1-k))); // adjust number of nested loops to keep precision with k approaching 1.000000. Watch the runtime!!
    if (N>=100000) N=100000;
    if (N<=0) N=1000000;           // otherwise the loop does not end
    delta=0.5*PI/N;
    for (i=0;i<=N;i++)
	{
	    x=sinq(i*delta);
	    sum+=sqrtq(1.0-k*powq(x,2));
	}
    sum=sum-0.5*(sqrtq(1.0-k*powq(x,2))+1.0);
    sum=sum*delta;
    return sum;
}
