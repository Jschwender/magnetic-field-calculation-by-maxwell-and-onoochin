#include <malloc.h>
#include <math.h>
#include "ellipke.cpp"
#include <quadmath.h>
#define float128 __float128

char sdr1[128], sdr2[128], sdr3[128], sdr4[128], serror[128];

void xprintf(char *sdring, float128 number) {
    quadmath_snprintf(sdring, 128, "%+-#20.*QE", 20, number);
}
void xprintfx(char *sdring, float128 number) {
    quadmath_snprintf(sdring, 128, "%+-#.*Qf", 6, number);
}

void print_IE(float128 k) {
float128 K, E, Ki, Ei;
        K_E_calc_AGM(k, K, E);  // AGM Method with 0…7 iteration steps
        Ei=Ecalc(k);   // numeric integration method
        Ki=Kcalc(k);
        xprintf(sdr1, k);
        xprintf(sdr2, Ki);
        xprintf(sdr3, E);
        xprintf(sdr4, Ei);
	printf(" %s %s,   %s %s %li\n", sdr1, sdr2, sdr3, sdr4, N);
}
int main()
{
//=== Compair numeric calculation of the complete elliptic integral
    float128 k=0.3, K1, Kweb, error;
    float128 Ek, Kk;
    printf("\n======= Compair numeric calculation of the complete elliptic integral ,E & K with AGM Method vs. numetic iteratiorn %li rounds========\n",N);
    printf("             k                       K_AGM                     K_iter                     E_AGM                   E_iter\n");
    print_IE(-20);
    print_IE(-5);
    print_IE(-4);
    print_IE(-3);
    print_IE(-2);
    print_IE(-1.0001);
    print_IE(-1);
    print_IE(-0.99999);
    print_IE(-0.9999);
    print_IE(-0.99);
    print_IE(-0.7);
    print_IE(-0.5);
    print_IE(0);
    print_IE(0.000001);
    print_IE(0.00001);
    print_IE(0.0001);
    print_IE(0.001);
    print_IE(0.01);
    print_IE(0.1);
    print_IE(0.2);
    print_IE(0.3);
    print_IE(0.4);
    print_IE(0.5); //
    print_IE(0.6);
    print_IE(0.7);
    print_IE(0.8);
    print_IE(0.9);
    print_IE(0.99);
    print_IE(0.999);
    print_IE(0.9999);
    print_IE(0.99999);
    print_IE(0.999999);
    print_IE(0.9999999);
    print_IE(0.99999999);
    print_IE(0.999999999);
    print_IE(0.9999999999);
    print_IE(0.99999999999);
    print_IE(0.999999999999);
    print_IE(0.9999999999999);
    print_IE(0.99999999999999);
    print_IE(0.999999999999999);
    print_IE(0.9999999999999999);
    print_IE(1.00);

printf("\n==========END=============\n"); //getchar();
return 0;
}
