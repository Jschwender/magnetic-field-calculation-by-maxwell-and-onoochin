#include <malloc.h>
#include <math.h>
#include "ellipke.cpp"
#include <quadmath.h>
#define float128 __float128

char sdr1[128], sdr2[128], sdr3[128], sdr4[128], serror[128];

void xprintf(char *sdring, float128 number) {
    quadmath_snprintf(sdring, 128, "%.*QE", 3, number);
}

/*	Calculates multilayer coil inductance L=length, Layers=layers Turns=turns in one layer,
    Di=inner diameter dwire=wire diameter td=separation between layers, . Note:td=dwire+d, d=dielectric thick 
    Wire round with maximum packaging t2=t1*0.866
*/
float128 Multilayer_L(float128 Length,long Layers,long TurnsPerLayer,float128 Di,float128 dw,float128 LayerInsulation);
/*
    Inductance of a solenoid by Maxwell’s method,using elliptic integrals Turns=turns, h=length,r=radius,b=0,dwire=wire diameter
*/
//Mutual inductance between coaxial coils radius r1,r2,dw=wire diameter, at a distance d between them
float128 Maxwell_loop_L(float128 r1,float128 r2,float128 distance,float128 w);

int main()
{
    printf("\n======= Multilayer coil inductance calculation according Maxwell and Vladimir Onoochin  ========\n");
/*==== 27.0 Calculates multilayer coil inductance L=length, C=layers T=turns in one layer, Di=inner diameter
	t=wire diameter t2=separation between layers, . Note:t2=t+d,d=dielectric thick
	Wire round with maximum packaging t2=t1*0.866
*/
    float128 Length=29.72e-3, Di=10e-3, dw=1.06e-3, LayerInsulation=.88e-3, L;
    long Layers=4, TurnsPerLayer=28;
    xprintf(sdr1, Length);
    xprintf(sdr2, Di);
    xprintf(sdr3, dw);
    xprintf(sdr4, LayerInsulation);
	    printf("Length: %s, Øi=%s, Øwire=%s, Layerinsulation=%s, Layers=%li, turns per layer=%li, total turns=%li ", sdr1, sdr2, sdr3, sdr4, Layers, TurnsPerLayer, Layers*TurnsPerLayer);
    L=Multilayer_L(Length,Layers,TurnsPerLayer,Di,dw,LayerInsulation);
    xprintf(sdr1, L);
    printf("L=%s H\n",sdr1);
    
printf("\n==========END=============\n"); //getchar();
return 0;
}
//10.0 === Functions ===================

/*	Calculates multilayer coil inductance L=length, C=layers T=turns in one layer, Di=inner diameter dwire=wire diameter td=separation between layers, . Note:td=t+d, d=dielectric
    thick Wire round with maximum packaging t2=t1*0.866
*/
float128 Multilayer_L(float128 Length,long Layers,long TurnsPerLayer,float128 Di,float128 dw,float128 LayerInsulation)
{
    long LayerTurn2,LayerTurn1,Layer1,Layer2;
    float128 L=0.0,z1,z2,r1,r2,dl;
    dl=1.0*(Length-LayerInsulation)/(TurnsPerLayer-1.0);//dl is the distance between a turn and the next one
    for (Layer1=0;Layer1<Layers;Layer1++)   // looping layer by layer
	{
//printf("layer Layer1:%ld          \n",Layer1);
	    r1=0.5*(Di + dw) + (LayerInsulation+dw)*Layer1;
	    for (LayerTurn1=0;LayerTurn1<TurnsPerLayer;LayerTurn1++)      // looping turn per turn within a layer
	    {
		z1=LayerTurn1*dl;
		for (Layer2=Layer1;Layer2<Layers;Layer2++)
		    {
//printf("  Layer2:%ld       \n",Layer2);
			r2=0.5*(Di + dw) + (LayerInsulation+dw)*Layer2;
			for (LayerTurn2=0;LayerTurn2<TurnsPerLayer;LayerTurn2++)
			    {
				z2=LayerTurn2*dl;
				float128 d=fabsq(z2-z1);
				float128 m;
				if ((Layer1==Layer2)&&(LayerTurn1==LayerTurn2))
				    {
					L+=Maxwell_loop_L(r1,r2,d,dw);

				    }
				else
					L+=2*Maxwell_loop_L(r1,r2,d,dw);
//			    L+=m;
			    }
		    }
	    }
	}
	return L;
}


/*12.0 === Auxiliar Functions ==========
*/
float128 Maxwell_loop_L(float128 r1,float128 r2,float128 distance, float128 dwire)
{
    float128 RM;
    RM=dwire/2*exp(-0.25);     //{g. m. d.}
    distance+=RM;
    long i,j;
    float128 a1,a2,da,x;
    da=PI*2/N;         //a1,a2=angles
    float128 r12=r1*r2;
    float128 r12d=powq(r1,2)+powq(r2,2)+powq(distance,2),cs;
    float128 sum=0.0;
    for (j=0;j<N;j++)
	{
	    a2=j*da;
	    cs=cosq(a2);
	    x=r12*cs/sqrtq(r12d-2.0*r12*cs);
	    sum+=x;
	}
    sum-=x/2;
    sum-=0.5*r12/sqrtq(r12d-2.0*r12); //quito la mitad de ambos extremos
    sum=1e-7*N*sum*powq(da,2);
    return sum;
}


